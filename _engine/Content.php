<?php
/**
 * Project: DorGen
 * User: Bagdad ( https://goo.gl/mRvZBa )
 * Date: 05.12.2015
 * Time: 10:06
 * Created by PhpStorm.
 */

namespace DorGen;


class Content
{
	static $linkPattern = array(
		"((http?:\/\/)" . OPTIONS_DONOR . "((?![^\" ]*(?:jpg|png|gif|jpeg|jfif))[^\" ]+))",
		"|(<link.*rel=.{1}canonical.{1}.*href=.{1}.*)(" . OPTIONS_DONOR . "\/)(.*/>)|iumu",
		"|(<option value=\"http:\/\/)(" . OPTIONS_DONOR . ")(\/.*\">.*<\/option>)|iusU",
	);

	static $ClassOptions = null;

	static $proxyList = array(
		'158.69.153.192:80',
		'104.209.140.210:8080',
		'66.76.24.115:3128',
		'88.150.234.120:3128'
	);

	function __construct($options = null)
	{
		self::$ClassOptions = $options;
	}

	/**
	 * Функция запроса содержимого страницы
	 *
	 * @param string $url
	 *
	 * @return string
	 */
	public function GetContent($url)
	{
		$gotContent = false;
		while($gotContent===false) {
			$curl = curl_init();

			$urlDetails = parse_url($url);

			$curlOptions = array();

			if (defined('OPTIONS_USEPROXY') && OPTIONS_USEPROXY === true) {
				$proxy = array_rand(self::$proxyList);
				$curlOptions[CURLOPT_PROXY] = self::$proxyList[$proxy];
				//echo(self::$proxyList[$proxy]);
				//die();
			}

			$curlOptions[CURLOPT_RETURNTRANSFER] = true;
			$curlOptions[CURLOPT_URL] = $url;
			$curlOptions[CURLOPT_TIMEOUT] = 300;
			$curlOptions[CURLOPT_FOLLOWLOCATION] = false;

			$curlOptions[CURLOPT_USERAGENT] = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36';
			if (isset(self::$ClassOptions['referer']) && self::$ClassOptions['referer'] !== false) {
				$curlOptions[CURLOPT_REFERER] = self::$ClassOptions['referer'];
			}
			if (self::$ClassOptions['ajax'] !== false) {
				$curlOptions[CURLOPT_HTTPHEADER] = array("X-Requested-With: XMLHttpRequest");
			}
			if (isset(self::$ClassOptions) && self::$ClassOptions['post'] === true) {
				if (isset($urlDetails['query'])) {
					$curlOptions[CURLOPT_POSTFIELDS] = $urlDetails['query'];
					$curlOptions[CURLOPT_HTTPHEADER] = array('Content-length:' . strlen($urlDetails['query']),
						'Content-Type: application/x-www-form-urlencoded',
						'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
						/* 'Cookie: Cookie:ASP.NET_SessionId=iwwmkffugdvjsi45s5wmxwmn; __utmt=1; _ym_visorc_7415752=w; _ym_visorc_6333970=w; _ga=GA1.2.1821460200.1427287094; _dc_gtm_UA-51412757-1=1; __utma=69487449.1821460200.1427287094.1427287094.1427348781.2; __utmb=69487449.2.9.1427348783251; __utmc=69487449; __utmz=69487449.1427287094.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)' */);
				}
				$curlOptions[CURLOPT_POST] = true;
			}
			curl_setopt_array($curl, $curlOptions);
			$pageContent = curl_exec($curl);

			$pageContent = mb_convert_encoding($pageContent, "utf-8", "utf-8");

			curl_close($curl);

			if(strpos($pageContent, "http://cdn1.elitebabes.com/")) {
				$gotContent = true;
			}

		}
		return $pageContent;
	}

	public function MakePath($PathString) {
		$NewPath = null;

		$parsedUrl = parse_url($PathString);

		if(isset($parsedUrl['scheme'])) {
			$NewPath = $parsedUrl['scheme'] . "://";
		} else {
			$NewPath = "http://";
		}

		if(isset($parsedUrl['host'])) {
			$NewPath .= $parsedUrl['host'];
		} else {
			$NewPath .= OPTIONS_DONOR;
		}

		if(isset($parsedUrl['path'])) {
			$NewPath .= str_replace('//', '/', $parsedUrl['path']);
		} else {
			$NewPath .= "/";
		}

		return $NewPath;
	}

	public function ContentDomainChanger($OriginalContent) {
		$newContent = null;

		if(isset($_SERVER['SERVER_NAME'])) {
			$myDomain = $_SERVER['SERVER_NAME'];
		} else {
			$myDomain = OPTIONS_DOMAIN;
		}

		$newContent = preg_replace(self::$linkPattern, '$1' . $myDomain . '$2', $OriginalContent);
		//echo($OriginalContent . "\r\n");
		//echo(self::$linkPattern . "\r\n");
		//echo($newContent . "\r\n");
		//$newContent = str_replace(OPTIONS_DONOR, $myDomain, $OriginalContent);

		return $newContent;
	}

}