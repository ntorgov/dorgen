<?php
/**
 * Project: DorGen
 * User: Bagdad ( https://goo.gl/mRvZBa )
 * Date: 07.12.2015
 * Time: 9:00
 * Created by PhpStorm.
 */

namespace DorGen;


class Replacer
{
	static $RulesDir = OPTIONS_SITEDIR . "/_replacements";

	/**
	 * Список файлов с правилами
	 * @var array
	 */
	static $RulesFileList = array();

	/**
	 * Список правил замены
	 * @var array
	 */
	static $RulesList = array();

	function __construct()
	{
		self::GetReplaceRules();
		self::LoadRules();
	}

	/**
	 * Функция получения списка правил из каталога
	 *
	 * @return mixed
	 */
	static function GetReplaceRules() {
		$list = null;
		$filesList = scandir(self::$RulesDir);

		foreach($filesList as $file) {
			if($file != '.' && $file != '..') {
				$list[] = $file;
			}
		}

		self::$RulesFileList = $list;

		return $list;
	}

	/**
	 * Функция загрузки правил замены из файлов
	 *
	 * @return array
	 */
	static function LoadRules() {
		$rules = array();

		foreach(self::$RulesFileList as $ruleFile) {
			$fileName = self::$RulesDir . '/' . $ruleFile;
			//$xmlContent = file_get_contents($fileName);
			$xml = simplexml_load_file($fileName, 'SimpleXMLElement', LIBXML_NOCDATA);

			$rule = trim($xml->search[0]);
			$type = mb_strtolower(trim($xml->search['type']));
			$replacement = trim($xml->replace[0]);
			$modifiers = trim($xml->search['modifiers']);

			//echo($xml->search['type'] . "\r\n");
			$rules[] = array(
				'search' => $rule,
				'type' => $type,
				'replace' => $replacement,
				'modifiers' => $modifiers
			);

			//print_r($rules);

		}

		self::$RulesList = $rules;

		return $rules;
	}

	/**
	 * Функция замены контента
	 * @param $Content
	 * @return mixed
	 */
	public function MakeReplacements($Content) {
		//print_r(self::$RulesList);
		foreach(self::$RulesList as $workingRule) {
			switch($workingRule['type']){
				case 'regexp':
					$workingPattern = "~".$workingRule['search']."~".$workingRule['modifiers'];

					$Content = preg_replace($workingPattern, $workingRule['replace'], $Content);
					//echo("Pattern: " . $workingPattern . "\r\n");
					//echo("Content: " . $Content . "\r\n");
					break;
				default:
					$Content = str_replace($workingRule['search'], $workingRule['replace'], $Content);
					break;
			}
		}

		return $Content;
	}

}