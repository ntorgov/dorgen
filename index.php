<?php
/**
 * Project: DorGen
 * User: Bagdad ( https://goo.gl/mRvZBa )
 * Date: 05.12.2015
 * Time: 8:42
 * Created by PhpStorm.
 */

require_once('config.inc.php');
require_once('_engine/Content.php');
require_once('_engine/Replacer.php');
require_once('_engine/Changer.php');

$contentClass = new \DorGen\Content();
$changerClass = new \DorGen\Changer();
$replacerClass = new \DorGen\Replacer();
//print_r($_SERVER['REQUEST_URI']);
$requestUrl = '';
if(isset($_SERVER['REQUEST_URI'])) {
	$requestUrl = $_SERVER['REQUEST_URI'];
}
$Path = $contentClass->MakePath($requestUrl);

$pathHash = md5($Path);

if(strpos($requestUrl, 'sitemap.xml')) {
	$PageContent = file_get_contents('_sitemap/sitemap.xml');
} else {

	if (file_exists('cache/' . $pathHash . '.php')) {
		$fileDate = filemtime('cache/' . $pathHash . '.php');
		$nowDate = date('U');
		$cacheDates = 7;
		if ($requestUrl == '/') {
			$cacheDates = 1;
		}
		$dateDiff = $cacheDates * 24 * 60 * 60;   // суточная разница во времени
		$criticalDate = $nowDate - $dateDiff;

		if ($fileDate < $criticalDate) {
			unlink('cache/' . $pathHash . '.php');
		}

	}
	if (!file_exists('cache/' . $pathHash . '.php')) {

//echo($Path);
		$ReceivedContent = $contentClass->GetContent($Path);

		$PageContent = $contentClass->ContentDomainChanger($ReceivedContent);
		$PageContent = $replacerClass->MakeReplacements($PageContent);


		//echo($PageContent);
		$PageContent = $changerClass->Clean($PageContent);
		//$PageContent = $changerClass->MinifyHtml($PageContent);

		file_put_contents('cache/' . $pathHash . '.php', $PageContent);
	} else {
		$PageContent = file_get_contents('cache/' . $pathHash . '.php');
	}
}

echo($PageContent);