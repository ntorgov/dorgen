<?php

/**
 * Project: DorGen
 * User: Bagdad ( https://goo.gl/mRvZBa )
 * Date: 07.12.2015
 * Time: 9:04
 * Created by PhpStorm.
 */

require_once("../config.inc.php");
require_once("../_engine/Replacer.php");

class ReplacerTest extends PHPUnit_Framework_TestCase
{
	private $classObject;

	function __construct()
	{
		$this->classObject = new \DorGen\Replacer();
	}

	public function test_GetReplaceRules() {
		//$classObject = new \DorGen\Replacer();

		$filesList = $this->classObject->GetReplaceRules();

		//print_r($filesList);

		$this->assertNotEmpty($filesList);
	}

	public function test_LoadRules() {
		$rules = $this->classObject->LoadRules();

		//print_r($rules);

		$this->assertNotEmpty($rules);
	}

	public function test_MakeReplacementsString() {
		$originalContent = 'Жопа<li class="a"><a accesskey="4" href="http://www.erosmatch.com/">Meet & Fuck</a> <em>(5)</em></li>жопа';

		$newContent = $this->classObject->MakeReplacements($originalContent);

		//print_r($newContent);

		$this->assertNotEquals($newContent, $originalContent);
	}

	public function test_MakeReplacementsRegexp() {
		$originalContent = 'Жопа<li class="a"><a accesskey="5" href="http://www.glamourwebcams.com/">Live Cams!</a> <em>(6)</em></li>жопа';

		$newContent = $this->classObject->MakeReplacements($originalContent);

		//print_r($newContent);

		$this->assertNotEquals($newContent, $originalContent);
	}
}
