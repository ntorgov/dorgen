<?php
/**
 * Project: DorGen
 * User: Bagdad ( https://goo.gl/mRvZBa )
 * Date: 05.12.2015
 * Time: 13:07
 * Created by PhpStorm.
 */

require_once("../config.inc.php");
require_once("../_engine/Changer.php");

class CangerTest extends PHPUnit_Framework_TestCase
{

	public function test_GoogleAnalyticsSimple() {
		$classObject = new \DorGen\Changer();

		$code = '/* ]]> */
</script>
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-50283154-4\', \'auto\');
  ga(\'send\', \'pageview\');

</script>
	</body>
';
		$newCode = $classObject->GoogleAnalytics($code);

		$this->assertNotEquals($newCode, $code);

	}

	public function test_Clean() {
		$classObject = new \DorGen\Changer();

		$code = '</body>
</html>
<!-- Quick Cache file built for (http://www.elitebabes.com/sofi-a-amento/) in 0.48064 seconds, on: Dec 5th, 2015 @ 7:11 am UTC. -->
<!-- This Quick ';

		$newCode = $classObject->Clean($code);

		$this->assertNotEquals($newCode, $code);
	}

	public function test_CleanUrl() {
		$classObject = new \DorGen\Changer();

		$code = '<a href="click.php?id=91187&amp;u=http://www.elitebabes.com/playboy-ukrainian-black-haired-beauty-malena-poses-for-the-camera/"><span class="img"><img src="http://cdn1.elitebabes.com/content/150717/ukrainian-black-haired-beauty-malena-poses-for-the-camera.jpg" width="200" height="260" border="0" alt="Malena mysterious girl"></span></a>';
		$newCode = $classObject->Clean($code);

		$this->assertNotEquals($newCode, $code);


		$this->assertNotEquals($newCode, $code);
		$code = '<a href="click.php?id=78902&u=http://dorgen.localhost/magnificent-julia-poses-naked-on-the-beach/"><span class="img"><img src="http://cdn1.elitebabes.com/content/141015/domai-julia-at-beach.jpg" width="200" height="260" border="0" alt="Julia magnificent julia poses naked on the beach"></span></a>';
		$newCode = $classObject->Clean($code);

		$this->assertNotEquals($newCode, $code);

		$code = '<a href="click.php?id=78902&u=http://dorgen.localhost/magnificent-julia-poses-naked-on-the-beach/"><span class="img"><img src="http://cdn1.elitebabes.com/content/141015/domai-julia-at-beach.jpg" width="200" height="260" border="0" alt="Julia magnificent julia poses naked on the beach"></span></a>
		<li><a href="click.php?id=92051&amp;u=http://dorgen.localhost/playboy-sensual-and-erotic-photo-session-by-some-amazing-looking-babes/"><span class="img"><img src="http://cdn1.elitebabes.com/content/150815/sensual-and-erotic-photo-session-by-some-amazing-looking-babes.jpg" width="200" height="260" border="0" alt="Sensual and erotic photo session by some amazing looking babes"></span></a></li>';
		$newCode = $classObject->Clean($code);

		print_r($newCode);

		$this->assertNotEquals($newCode, $code);
	}
}
