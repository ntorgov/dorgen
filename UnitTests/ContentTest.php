<?php
/**
 * Project: DorGen
 * User: Bagdad ( https://goo.gl/mRvZBa )
 * Date: 05.12.2015
 * Time: 10:04
 * Created by PhpStorm.
 */

require_once("../config.inc.php");
require_once("../_engine/Content.php");

class ContentTest extends PHPUnit_Framework_TestCase
{

	public function test_MakePathSimple() {
		$classObject = new \DorGen\Content();

		$path = "https://ya.ru/index.php";
		$validPath = "https://ya.ru/index.php";

		$newPath = $classObject->MakePath($path);

		$this->assertEquals($validPath, $newPath);
	}

	public function test_MakePathExcessSlashes() {
		$classObject = new \DorGen\Content();

		$path = "https://ya.ru//index.php/";
		$validPath = "https://ya.ru/index.php";

		$newPath = $classObject->MakePath($path);

		$this->assertEquals($validPath, $newPath);
	}

	public function test_ContentDomainChangerSimple() {
		$classObject = new \DorGen\Content();

		//$content = "<a href=\"http://" . OPTIONS_DONOR . "\">Задница</a>";
		//$validContent = "<a href=\"http://" . OPTIONS_DOMAIN . "\">Задница</a>";
		//$newContent = $classObject->ContentDomainChanger($content);

		//$this->assertEquals($validContent, $newContent);

		$content = "<a href=\"http://" . OPTIONS_DONOR . "/\">Задница</a>";
		$validContent = "<a href=\"http://" . OPTIONS_DOMAIN . "/\">Задница</a>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);

		$content = "<a href=\"http://" . OPTIONS_DONOR . "/simple/\">Задница</a>";
		$validContent = "<a href=\"http://" . OPTIONS_DOMAIN . "/simple/\">Задница</a>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);


		$content = "<a href=\"http://".OPTIONS_DONOR ."/model/emily-bloom/\">Emily Bloom</a>";
		$validContent = "<a href=\"http://".OPTIONS_DOMAIN ."/model/emily-bloom/\">Emily Bloom</a>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);

		$content = "<link rel='canonical' href='http://".OPTIONS_DONOR."/' />";
		$validContent = "<link rel='canonical' href='http://".OPTIONS_DOMAIN."/' />";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);

		$content = "<li><a href=\"http://www.elitebabes.com/most-viewed/\">Best Galleries</a></li>";
		$validContent = "<li><a href=\"http://".OPTIONS_DOMAIN."/most-viewed/\">Best Galleries</a></li>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);

		$content = "<li><a href=\"click.php?id=64401&u=http://www.elitebabes.com/violin-lesson/\"><img src=\"http://cdn1.elitebabes.com/content/140412/domai-violin-lesson.jpg\" width=\"200\" height=\"260\" border=\"0\" alt=\"Jane�doe violin lesson\" /></a></li>";
		$validContent = "<li><a href=\"click.php?id=64401&u=http://".OPTIONS_DOMAIN."/violin-lesson/\"><img src=\"http://cdn1.elitebabes.com/content/140412/domai-violin-lesson.jpg\" width=\"200\" height=\"260\" border=\"0\" alt=\"Jane�doe violin lesson\" /></a></li>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);

		$content = "<li><a href=\"http://www.elitebabes.com/content/141015/domai-pela-posing-naked-01.jpg\"><img src=\"http://www.elitebabes.com/content/141015/domai-pela-posing-naked-01t.jpg\" width=\"170\" height=\"250\" alt=\"\"></a></li>";
		$validContent = "<li><a href=\"http://www.elitebabes.com/content/141015/domai-pela-posing-naked-01.jpg\"><img src=\"http://www.elitebabes.com/content/141015/domai-pela-posing-naked-01t.jpg\" width=\"170\" height=\"250\" alt=\"\"></a></li>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);

		$content = "<li><a href=\"http://www.elitebabes.com/domai/\">Domai</a></li>";
		$validContent = "<li><a href=\"http://".OPTIONS_DOMAIN."/domai/\">Domai</a></li>";
		$newContent = $classObject->ContentDomainChanger($content);

		$this->assertEquals($validContent, $newContent);
	}
}
